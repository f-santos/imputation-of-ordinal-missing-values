Imputation of ordinal missing data: a case study for entheseal changes
======================================================================

<a rel="license" href="http://creativecommons.org/licenses/by-nc-sa/4.0/"><img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by-nc-sa/4.0/88x31.png" /></a><br />This work is licensed under a <a rel="license" href="http://creativecommons.org/licenses/by-nc-sa/4.0/">Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International License</a>.

**Authors**: Frédéric Santos, Sébastien Villotte.

**Description of the study**: This GitLab repository is a companion for a short response to an article by Wissler et al. (2022), *Missing data in bioarchaeology II*, [doi: 10.1002/ajpa.24614 ](https://doi.org/10.1002/ajpa.24614). This short response, published in the *American Journal of Biological Anthropology* (doi: [10.1002/ajpa.24860](https://doi.org/10.1002/ajpa.24860)), is a comment about, and a complement to, the results obtained by Wissler et al. in their article.

## Contents

This repository is organized as follows:

- The folder `analysis` contains various R script files allowing for the replications of our own results.
- The folder `data_wissler` contains the original data file by Wissler et al. (2022), along with several data files where missing values were added at random.
- The folders `data_F` and `data_FC` contain similar data files for fibrous and fibrocartilaginous entheseal changes, respectively.
- The folder `src` contains custom R functions written for this study.

## Details and technical information

- The R scripts were written using R 4.3.1. All packages versions are handled with `{groundhog}`. To install the R package `{groundhog}`, you just have to execute:
  ```{r}
  install.packages("groundhog")
  ```
- The R scripts in the `analysis` folder can be run one by one in the suggested order; but each script can also be run independently (in the later case, the results from the previous scripts are simply retrieved upstream).
- The R script `analysis/07_supplementary_analyses.R` is the only one that presents additional results, which are not shown in the AJBA manuscript. Its main aim is to show that the results and conclusions drawn used the PPM method still hold for another imputation method tested in Wissler et al.'s original article (Random forests). The corresponding result is given as `Figure3.png` in the folder `figures`.
- The folder `data_FC` is provided for information only: the analyses presented in our article only make use of the fibrous entheseal dataset.
