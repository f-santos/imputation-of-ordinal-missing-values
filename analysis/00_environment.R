library(groundhog)

## Load (and install) all required packages:
gdate <- "2023-07-10"

groundhog.library(pkg = "tidyverse", date = gdate)
groundhog.library(pkg = "cowplot", date = gdate)
groundhog.library(pkg = "here", date = gdate)
groundhog.library(pkg = "hydroGOF", date = gdate)
groundhog.library(pkg = "imputeR", date = gdate)
groundhog.library(pkg = "mice", date = gdate)

## Load custom R functions:
source(here("src", "eval_metrics.R"))
source(here("src", "imputation.R"))
source(here("src", "load_data.R"))
source(here("src", "simu_NA.R"))

## Set working directory:
here::i_am("./analysis/00_environment.R")
