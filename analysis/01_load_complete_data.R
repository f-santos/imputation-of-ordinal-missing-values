###########################
### Restore environment ###
###########################
library(groundhog)
groundhog.library(pkg = "here", date = "2023-07-10")
here::i_am("./analysis/01_load_complete_data.R")
source(here("analysis", "00_environment.R"))

#################
### Load data ###
#################
## Fibrous entheses:
compf <- read.csv2(
    file = here("data_F", "complete", "data_fibrous.csv"),
    row.names = 1,
    stringsAsFactors = TRUE,
    na.strings = "",
    fileEncoding = "utf-8"
)|>
    select(ends_with("_R"))

summary(compf)

## Wissler et al. (2022) data:
compw <- read.csv2(
    file = here("data_wissler", "complete", "data_wissler.csv"),
    row.names = 1,
    stringsAsFactors = TRUE,
    na.strings = "",
    fileEncoding = "utf-8"
)

summary(compw)

## [Bonus] Fibrocartilaginous entheses (not used in the article):
compfc <- read.csv2(
    file = here("data_FC", "complete", "data_fibrocartilaginous.csv"),
    row.names = 1,
    stringsAsFactors = TRUE,
    na.strings = "",
    fileEncoding = "utf-8"
)|>
    select(ends_with("_R"))

summary(compfc)
